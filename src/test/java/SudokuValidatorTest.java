import java.io.File;
import java.io.FileNotFoundException;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;


public class SudokuValidatorTest {
    private ClassLoader cl = getClass().getClassLoader();

    private int[][]validSudoku ={{ 4,1,7,3,6,9,8,2,5 },
            { 6,3,2,1,5,8,9,4,7 },
            { 9,5,8,7,2,4,3,1,6 },
            { 8,2,5,4,3,7,1,6,9 },
            { 7,9,1,5,8,6,4,3,2 },
            { 3,4,6,9,1,2,7,5,8 },
            { 2,8,9,6,4,3,5,7,1 },
            { 5,7,3,2,9,1,6,8,4 },
            { 1,6,4,8,7,5,2,9,3 }};

    private int[][]invalidSudoku ={{ 1,1,7,3,6,9,8,1,5 },
            { 6,3,2,1,5,8,9,4,7 },
            { 9,5,8,7,2,4,3,1,6 },
            { 8,2,5,4,3,7,1,6,9 },
            { 7,9,1,5,8,6,4,3,2 },
            { 3,4,6,9,1,2,7,5,8 },
            { 2,8,9,6,4,3,5,7,1 },
            { 5,7,3,2,9,1,6,8,4 },
            { 1,6,4,8,7,5,2,9,3 }};

    @Test
    public void sudokuValid(){
        assertTrue(new SudokuValidator().isSudokuValid(validSudoku));
    }

    @Test
    public void sudokuInvalid(){
        assertFalse(new SudokuValidator().isSudokuValid(invalidSudoku));
    }

    @Test
    public void readSudokuCSVInvalidValuesNotInRange(){
        try {
            File file = new File(cl.getResource("sudokuInvalidValuesNotInRange.txt").getFile());
            new SudokuValidator().readSudokuCSV(file);
        }
        catch (NumberFormatException | FileNotFoundException exception) {
            assertThat(exception.getMessage(), containsString("Sudoku values must be in range 1..9, got:"));
        }
    }

    @Test (expected = NumberFormatException.class)
    public void readSudokuCSVInvalidNotIntValues() throws FileNotFoundException {
        File file = new File(cl.getResource("sudokuInvalidNotIntValues.txt").getFile());
        new SudokuValidator().readSudokuCSV(file);
    }

    @Test(expected = FileNotFoundException.class)
    public void readSudokuCSVFileIsMissing() throws FileNotFoundException {
        File file = new File("sudokuCSVFileIsMissing.txt");
        new SudokuValidator().readSudokuCSV(file);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void readSudokuCSVArraySizeMismatch() throws FileNotFoundException {
        File file = new File(cl.getResource("sudokuCSVArraySizeMismatch.txt").getFile());
        new SudokuValidator().readSudokuCSV(file);
    }
}
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class SudokuValidator {
    public boolean isSudokuValid(int[][] grid) {
        for (int i = 0; i < 9; i++) {

            int[] box = new int[9];
            int[] row = new int[9];
            int[] column = new int[9];

            for (int j = 0; j < 9; j ++) {
                box[j] = grid[(i / 3) * 3 + j / 3][i * 3 % 9 + j % 3];
                row[j] = grid[j][i];
                column[j] = grid[i][j];
            }
            if (!(validate(box) && validate(row)&& validate(column)))
                return false;
        }
        return true;
    }

    private boolean validate(int[] set) {
        int[] result = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        Arrays.sort(set);
        return Arrays.equals(set, result);
    }

    public int[][] readSudokuCSV(File file)
            throws java.io.FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader(file)));
        int rows = 9;
        int columns = 9;
        int [][] sudoku = new int[rows][columns];
        int value;
        while(sc.hasNextLine()) {
            for (int i=0; i<sudoku.length; i++) {
                String[] line = sc.nextLine().trim().split(",");
                for (int j=0; j<line.length; j++) {
                    value = Integer.parseInt(line[j]);
                    if (value>=1 && value <=9) {
                        sudoku[i][j] = value;
                    }
                    else {
                        throw new NumberFormatException("Sudoku values must be in range 1..9, got: " + value);
                    }
                }
            }
        }
        sc.close();
        return sudoku;
    }

    public static void main(String[] args) {
        try {
            File file = new File(args[0]);
            SudokuValidator sv = new SudokuValidator();
            int[][] sudoku = sv.readSudokuCSV(file);
            System.out.println(sv.isSudokuValid(sudoku) ? "VALID" : "INVALID");
        }
        catch (IOException | NumberFormatException | ArrayIndexOutOfBoundsException e){
            System.out.println("INVALID");
            e.printStackTrace();
        }
    }
}